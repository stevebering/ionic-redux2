/**
 * Created by sbering on 4/22/2016.
 */

import * as actionTypes from '../constants/actionTypes';
import * as _ from 'lodash';

import { getJSONFromLocalStorage } from './localStoragePersister';

const initialAppStatus = {
    hasStarted: false,
    isClosing: false,
    isLoading: false,
};

export interface IAppState {
  token: any,
  appStatus: any,
  todos: any[]
}

export let initialState: IAppState = {
    token: undefined,
    appStatus: initialAppStatus,
    todos: []
};

function brioApp(state = initialState, action) {
    let newState = {
        token: token(state.token, action),
        appStatus: appStatus(state, action),
        todos: todos(state.todos, action),
    };

    return newState;
}

function token(state = null, action) {
    switch (action.type) {
        case actionTypes.SET_TOKEN: {
            return action.payload;
        }
        case actionTypes.SIGN_OUT: {
            return null;
        }
        case actionTypes.START_APP: {
            let tokenInfo = getJSONFromLocalStorage('token', {});
            if (tokenInfo == null || tokenInfo.authToken == null || tokenInfo.expires < new Date()) {
                return null;
            }

            return tokenInfo.authToken;
        }
        default: {
            return state;
        }
    }
}

function todos(state = [], action) {
  switch (action.type) {
    case actionTypes.ADD_TODO: {
      return [ ...state, action.todo ]
    }
    case actionTypes.REMOVE_TODO: {
      return _.filter(state, (todo) => {
        return todo !== action.todo;
      });
    }
    default: {
      return state;
    }
  }
}

function appStatus(state = initialState, action) {
    // we take the entire state model here, because we want to
    // make decisions on state based on the entire model
    let appStatus = state.appStatus;
    switch (action.type) {
        case actionTypes.SET_TOKEN: {
            return _.assign({}, appStatus, {
                hasStarted: false,
                isClosing: false,
                isLoading: true,
            });
        }
        case actionTypes.SIGN_OUT: {
            return _.assign({}, appStatus, {
                hasStarted: false,
                isClosing: true,
                isLoading: true,
            });
        }
        case actionTypes.CLEAR_LOCAL_DATA: {
            return _.assign({}, appStatus, {
                hasStarted: false,
                isClosing: true,
                isLoading: true,
            });
        }
        case actionTypes.START_APP: {
            return _.assign({}, appStatus, {
                hasStarted: true,
                isClosing: false,
                isLoading: true,
            });
        }
        default: {
            return _.assign({}, appStatus);
        }
    }
}

export default brioApp;
