/**
 * Created by sbering on 4/27/2016.
 */
export function getJSONFromLocalStorage(key: string, defaultValue: any) : any {
    var storedItem = window.localStorage.getItem(key);
    if (!storedItem) {
        return defaultValue;
    }

    return JSON.parse(storedItem);
}
