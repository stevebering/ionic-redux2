import { Component } from '@angular/core';
import { select, NgRedux } from 'ng2-redux';
import { NavController, ModalController } from 'ionic-angular';
import { IAppState } from '../../reducers/rootReducer';
import { Observable } from "rxjs";
import { AddItemPage } from '../add-item-page/add-item-page';
import { ItemDetailPage} from '../item-detail-page/item-detail-page';
import * as actionTypes from '../../constants/actionTypes';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public items: any[] = [];

  /**
   * Observable tied to todos property on appState.
   * The subscribe callback is fired every time this property changes
   */
  @select() todos$: Observable<any>;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private ngRedux: NgRedux<IAppState>) { }

  ionViewDidLoad() {
    this.todos$.subscribe(todos => {
      this.items = todos;
    })
  }

  addItem() {
    let addModal = this.modalCtrl.create(AddItemPage);
    addModal.onDidDismiss((item) => {
      if (item) {
        this.saveItem(item);
      }
    });

    addModal.present();
  }

  saveItem(item) {
    this.ngRedux.dispatch({
      type: actionTypes.ADD_TODO,
      todo: item
    });
  }

  viewItem(item) {
    this.navCtrl.push(ItemDetailPage, {
      item: item
    });
  }
}
