/**
 * Created by sbering on 4/22/2016.
 */
// user actions
export const SET_TOKEN = 'SET_TOKEN';
export const SIGN_OUT = 'SIGN_OUT';

// todo actions
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';

// app actions
export const CLEAR_LOCAL_DATA = 'CLEAR_LOCAL_DATA';
export const START_APP = 'START_APP';
export const APP_STARTING = 'APP_STARTING';
export const APP_STARTED = 'APP_STARTED';
export const REDIRECT_REQUESTED = 'REDIRECT_REQUESTED';
export const REDIRECT_COMPLETED = 'REDIRECT_COMPLETED';

// facility actions
export const REQUEST_FACILITIES = 'REQUEST_FACILITIES';
export const RECEIVE_FACILITIES = 'RECEIVE_FACILITIES';
export const CLAIM_FACILITY = 'CLAIM_FACILITY';

// team actions
export const REQUEST_TEAMS = 'REQUEST_TEAMS';
export const RECEIVE_TEAMS = 'RECEIVE_TEAMS';
export const REQUEST_CLAIM_TEAM = 'REQUEST_CLAIM_TEAM';
export const RECEIVE_CLAIM_TEAM = 'RECEIVE_CLAIM_TEAM';
export const RELEASE_TEAM_CLAIM = 'RELEASE_TEAM_CLAIM';

// stay actions
export const REQUEST_STAY_DETAILS = 'REQUEST_STAY_DETAILS';
export const RECEIVE_STAY_DETAILS = 'RECEIVE_STAY_DETAILS';
export const REQUEST_PATIENT_DETAILS = 'REQUEST_PATIENT_DETAILS';
export const RECEIVE_PATIENT_DETAILS = 'RECEIVE_PATIENT_DETAILS';
export const RECEIVE_STAY_DETAILS_COMPLETE = 'RECEIVE_STAY_DETAILS_COMPLETE';

// profile actions
export const REQUEST_PROFILE = 'REQUEST_PROFILE';
export const RECEIVE_PROFILE = 'RECEIVE_PROFILE';

// task actions
export const REQUEST_STAY_TASKS = 'REQUEST_STAY_TASKS';
export const RECEIVE_STAY_TASKS = 'RECEIVE_STAY_TASKS';

// admission actions
export const ADMISSION_START_ADMISSION = 'ADMISSION_START_ADMISSION';
export const ADMISSION_SELECT_PATIENT = 'ADMISSION_SELECT_PATIENT';
export const ADMISSION_SELECT_FACILITY = 'ADMISSION_SELECT_FACILITY';
export const ADMISSION_SELECT_TEAM = 'ADMISSION_SELECT_TEAM';
export const ADMISSION_STAY_CREATED = 'ADMISSION_STAY_CREATED';
export const ADMISSION_CANCEL = 'ADMISSION_CANCEL';
export const ADMISSION_SEARCH_PATIENTS = 'ADMISSION_SEARCH_PATIENTS';
export const ADMISSION_STAY_PREVIOUSLY_CREATED = 'ADMISSION_STAY_PREVIOUSLY_CREATED';

